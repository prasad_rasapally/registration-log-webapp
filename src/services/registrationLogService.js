import RegistrationLogServiceSdk from 'registration-log-service-sdk';

export default class RegistrationLogService {

    _$q;
    _registrationLogServiceSdk : RegistrationLogServiceSdk

    constructor(
        $q,
        registrationLogServiceSdk : RegistrationLogServiceSdk
    ){        
        this.getRegistrationLogs = function( accountId, accessToken ){
            return $q( resolve =>
                registrationLogServiceSdk
                .listRegistrationLogWithId( accountId, accessToken )
                    .then(
                        registrationLogs =>
                            resolve(registrationLogs)
                    ).catch(function(error){
                        console.log("error in registrationLogs......", error);
                    })
            )
        }
    }
}

RegistrationLogService.$inject = [
    '$q',
    'registrationLogServiceSdk'
];