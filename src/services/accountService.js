import AccountServiceSdk from 'account-service-sdk';

export default class AccountService {

    _$q;
    _accountServiceSdk : AccountServiceSdk;

    constructor(
        $q,
        accountServiceSdk : AccountServiceSdk
    ){        
        this.getAccountInfo =
            function( accountId, accessToken ){
                return $q( resolve =>
                        accountServiceSdk
                            .getCommercialAccountWithId( accountId, accessToken )
                            .then(commercialAccount =>
                                resolve(commercialAccount)
                            ).catch(function(error){
                            console.log("error in commercialAccount......", error);
                            })
                        )};
    }
}

AccountService.$inject = [
    '$q',
    'accountServiceSdk'
];