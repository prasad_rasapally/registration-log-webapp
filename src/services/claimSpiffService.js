import ClaimSpiffServiceSdk from 'claim-spiff-service-sdk';

export default class ClaimSpiffService {

    _$q;
	
    _claimSpiffServiceSdk : ClaimSpiffServiceSdk;

    constructor(
        $q,
        claimSpiffServiceSdk : ClaimSpiffServiceSdk
    ){ 
		if(!$q){
            throw new TypeError('$q required');
        }
        this._$q = $q;
		
		if(!claimSpiffServiceSdk){
            throw new TypeError('claimSpiffServiceSdk required');
        }
        this._claimSpiffServiceSdk = claimSpiffServiceSdk;        
    }
	
	getClaimSpiffInfo ( registrationId, accessToken ){
		return this._$q( resolve =>
			this._claimSpiffServiceSdk
			.getEntitlementWithPartnerSaleRegistrationId( registrationId, accessToken )
				.then(
					claimSpiffs =>
						resolve(claimSpiffs)
				).catch(function(error){
					console.log("error in ClaimSpiffService......", error);
				})
		)
	};	
	
	/*backFillSpiffDetailsService( accessToken ){
		return this._$q( resolve =>
			this._claimSpiffServiceSdk
				.backFillSpiffDetails( accessToken )
				.then(() => resolve(true))
				.catch(function(error){
						console.log("Error in backFillDetails", error);
					}
			  	)
		)
	};*/
}

ClaimSpiffService.$inject = [
    '$q',
    'claimSpiffServiceSdk'
];