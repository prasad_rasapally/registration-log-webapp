import PartnerRepServiceSdk from 'partner-rep-service-sdk';

/**
 * @class {PartnerRepService}
 */

export default class PartnerRepService {

    _$q;

    _partnerRepServiceSdk : PartnerRepServiceSdk;

    constructor(
        $q,
        partnerRepServiceSdk : PartnerRepServiceSdk
    ){        
        this.getDealerRep = function( partnerRepId, accessToken ){
            return $q(
                (resolve, reject) =>
                    partnerRepServiceSdk
                        .getPartnerRepWithId( partnerRepId, accessToken )
                        .then(partnerReps =>
                            resolve(partnerReps)
                        ).catch( error => {
                               reject(error)
                            }
                        )
            )
        }
    }
}

PartnerRepService.$inject = [
    '$q',
    'partnerRepServiceSdk'
];