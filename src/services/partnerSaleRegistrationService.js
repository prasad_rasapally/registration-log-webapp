import PartnerSaleRegistrationServiceSdk from 'partner-sale-registration-draft-service-sdk';
import AccountContactServiceSdk from 'account-contact-service-sdk';
import BuyingGroupServiceSdk from 'buying-group-service-sdk';
import ManagementCompanyServiceSdk from 'management-company-service-sdk';
import CustomerSourceServiceSdk from 'customer-source-service-sdk';

export default class PartnerSaleRegistrationService {

    _$q;
    
    _partnerSaleRegistrationServiceSdk : PartnerSaleRegistrationServiceSdk;
    
    _accountContactServiceSdk : AccountContactServiceSdk;
    
    _buyingGroupServiceSdk : BuyingGroupServiceSdk;
    
    _managementCompanyServiceSdk : ManagementCompanyServiceSdk;
    
    _customerSourceServiceSdk : CustomerSourceServiceSdk;

    constructor(
        $q,
        
        partnerSaleRegistrationServiceSdk : PartnerSaleRegistrationServiceSdk,
        
        accountContactServiceSdk : AccountContactServiceSdk,
        
        buyingGroupServiceSdk : BuyingGroupServiceSdk,
        
        managementCompanyServiceSdk : ManagementCompanyServiceSdk,
        
        customerSourceServiceSdk : CustomerSourceServiceSdk        
    ){

        this.getPartnerSaleRegistrationInfo =
            function( draftId, accessToken ){
                return $q( resolve =>
                        partnerSaleRegistrationServiceSdk
                            .getPartnerSaleRegistrationDraftWithDraftId( draftId, accessToken )
                            .then(partnerSaleRegistrationInfo =>
                                    resolve(partnerSaleRegistrationInfo)
                            ).catch(function(error){
                            console.log("error in partnerSaleRegistrationInfo......", error);
                            })
                        )};

        this.getPartnerSaleLineItemsExcludeSerialNumberWithDraftId =
            function( draftId, accessToken ){
                return $q( resolve =>
                        partnerSaleRegistrationServiceSdk
                            .getPartnerSaleLineItemsExcludeSerialNumberWithDraftId( draftId, accessToken )
                            .then(partnerSaleRegistrationInfo =>
                                resolve(partnerSaleRegistrationInfo)
                            ).catch(function(error){
                                console.log("error in partnerSaleRegistrationInfo......", error);
                            })
                       )};

        
        this.getPartnerContactInfo =
            function( facilityContactId , accessToken ){
                return $q( resolve =>
                       accountContactServiceSdk
                            .getAccountContactWithId( facilityContactId , accessToken )
                            .then( response =>
                                resolve(response)
                            ).catch( function( error ){
                                console.log("error in facilityContacts......",error);
                            })
                       )};

        
        this.getBuyingGroupInfo =
            function( accessToken ){
                return $q(resolve =>
                        buyingGroupServiceSdk
                            .listBuyingGroups( accessToken )
                            .then(response =>
                                resolve(response)
                            )
                        )};

        this.getManagementCompanyInfo =
            function( accessToken ){
                return $q(resolve =>
                        managementCompanyServiceSdk
                            .listManagementCompanies(accessToken)
                            .then(response =>
                                resolve(response)
                            )
                        )};

        this.getCustomerSourceInfo =
            function( accessToken ){
                return $q(resolve =>
                        customerSourceServiceSdk
                            .listCustomerSources( accessToken )
                            .then(response =>
                                resolve(response)
                            )
                        )};

        }
}

PartnerSaleRegistrationService.$inject = [
    '$q',
    'partnerSaleRegistrationServiceSdk',
    'accountContactServiceSdk',
    'buyingGroupServiceSdk',
    'managementCompanyServiceSdk',
    'customerSourceServiceSdk'
];