import SpiffMasterDataServiceSdk from 'master-data-service-sdk';

/**
 * @class {SpiffMasterDataService}
 */

export default class SpiffMasterDataService {

    _$q;
    
    _spiffMasterDataServiceSdk : SpiffMasterDataServiceSdk;

    constructor(
        $q,
        
        spiffMasterDataServiceSdk : SpiffMasterDataServiceSdk
    ){
        /**
         * initialization
         */
        if(!$q){
            throw new TypeError("$q required");
        }
        this._$q = $q;
        
        if(!spiffMasterDataServiceSdk){
            throw new TypeError("spiffMasterDataServiceSdk required");
        }
        this._spiffMasterDataServiceSdk = spiffMasterDataServiceSdk;
    }
    
    /**
     * methods
     */
    getSpiffAmountsWithSerialNumbers( request , accessToken ){
        return this._$q( ( resolve , reject ) => {
                this._spiffMasterDataServiceSdk.spiffAmountSerialNumber( request , accessToken )
                .then( response => 
                        resolve( response )                    
                ).catch( error => 
                    reject("Error in loading SPIFF Data" + error)
                )
            }
        )
    }
}

SpiffMasterDataService.$inject = [
    '$q',
    'spiffMasterDataServiceSdk'
];