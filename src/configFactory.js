import Config from './config';
import {IdentityServiceSdkConfig} from 'identity-service-sdk';
import {SessionManagerConfig} from 'session-manager';
import {RegistrationLogServiceSdkConfig} from 'registration-log-service-sdk';
import {PartnerSaleRegistrationDraftServiceSdkConfig} from 'partner-sale-registration-draft-service-sdk';
import {ConsumerSaleRegistrationServiceSdkConfig} from 'consumer-sale-registration-service-sdk';
import {AccountServiceSdkConfig} from 'account-service-sdk';
import {AccountContactServiceSdkConfig} from 'account-contact-service-sdk';
import {BuyingGroupServiceSdkConfig} from 'buying-group-service-sdk';
import {ManagementCompanyServiceSdkConfig} from 'management-company-service-sdk';
import {CustomerSourceServiceSdkConfig} from 'customer-source-service-sdk';
import {PartnerRepServiceSdkConfig} from 'partner-rep-service-sdk';
import {ClaimSpiffServiceSdkConfig} from 'claim-spiff-service-sdk';
import {ExtendedWarrantyServiceSdkConfig} from 'extended-warranty-service-sdk';
import {SpiffMasterDataServiceSdkConfig} from 'master-data-service-sdk';

export default class ConfigFactory {

    /**
     * @param {object} data
     * @returns {Config}
     */
    static construct(data):Config {

        const identityServiceSdkConfig =
            new IdentityServiceSdkConfig(
                data.precorConnectApiBaseUrl
            );

        const sessionManagerConfig =
            new SessionManagerConfig(
                data.precorConnectApiBaseUrl,
                data.sessionManagerConfig.loginUrl,
                data.sessionManagerConfig.logoutUrl
            );

        const registrationLogServiceSdkConfig = 
              new RegistrationLogServiceSdkConfig(
                  data.precorConnectApiBaseUrl
              );
        
        const partnerSaleRegistrationDraftServiceSdkConfig = 
              new PartnerSaleRegistrationDraftServiceSdkConfig(
                  data.precorConnectApiBaseUrl
              );
        
        const consumerSaleRegistrationServiceSdkConfig = 
              new ConsumerSaleRegistrationServiceSdkConfig(
                  data.precorConnectApiBaseUrl
              );
        
        const accountServiceSdkConfig = 
              new AccountServiceSdkConfig(
                  data.precorConnectApiBaseUrl
              );
        
        const accountContactServiceSdkConfig = 
              new AccountContactServiceSdkConfig(
                  data.precorConnectApiBaseUrl
              );
        
        const buyingGroupServiceSdkConfig = 
              new BuyingGroupServiceSdkConfig(
                  data.precorConnectApiBaseUrl
              );
        
        const managementCompanyServiceSdkConfig = 
              new ManagementCompanyServiceSdkConfig(
                  data.precorConnectApiBaseUrl
              );
        
        const customerSourceServiceSdkConfig = 
              new CustomerSourceServiceSdkConfig(
                  data.precorConnectApiBaseUrl
              );
        
        const partnerRepServiceSdkConfig = 
              new PartnerRepServiceSdkConfig(
                  data.precorConnectApiBaseUrl
              );
        
        const claimSpiffServiceSdkConfig = 
              new ClaimSpiffServiceSdkConfig(
                  data.precorConnectApiBaseUrl
              );
        
        const extendedWarrantyServiceSdkConfig = 
              new ExtendedWarrantyServiceSdkConfig(
                  data.precorConnectApiBaseUrl
              );
        
        const spiffMasterDataServiceSdkConfig = 
              new SpiffMasterDataServiceSdkConfig(
                  data.precorConnectApiBaseUrl
              );
        
        return new Config(
            identityServiceSdkConfig,
            sessionManagerConfig,
            registrationLogServiceSdkConfig,
            partnerSaleRegistrationDraftServiceSdkConfig,
            consumerSaleRegistrationServiceSdkConfig,
            accountServiceSdkConfig,
            accountContactServiceSdkConfig,
            buyingGroupServiceSdkConfig,
            managementCompanyServiceSdkConfig,
            customerSourceServiceSdkConfig,
            partnerRepServiceSdkConfig,
            claimSpiffServiceSdkConfig,
            extendedWarrantyServiceSdkConfig,
            spiffMasterDataServiceSdkConfig
        );
    }

}
