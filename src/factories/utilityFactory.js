import PrepareClaimSpiffRequestFactory from "./prepareClaimSpiffRequestFactory";
import MergeSpiffDataWithAssetsFactory from "./mergeSpiffDataWithAssetsFactory";

angular.module('registrationLogWebApp.factories', [])
    .factory('prepareClaimSpiffRequestFactory', PrepareClaimSpiffRequestFactory.prepareClaimSpiffRequestFactory)
    .factory('mergeSpiffDataWithAssetsFactory', MergeSpiffDataWithAssetsFactory.mergeSpiffDataWithAssetsFactory);