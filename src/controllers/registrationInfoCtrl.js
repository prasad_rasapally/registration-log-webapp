export default class RegistrationInfoCtrl {
    
    _$scope;
    
    _$q;
    
    _identityService;
    
    _partnerSaleRegistrationService;
    
    _consumerSaleRegistrationService;
    
    _accountService;
    
    _partnerRepService;

    constructor(
        $scope,
        $q,
        identityService,
        partnerSaleRegistrationService,
        consumerSaleRegistrationService,
        accountService,
        partnerRepService
    ){  
        /**
         * initialization
         */
        if(!$scope){
            throw new TypeError('$scope required');
        }
        this._$scope = $scope;
        
        if(!$q){
            throw new TypeError('$q required');
        }
        this._$q = $q;
        
        if(!identityService){
            throw new TypeError('identityService required');
        }
        this._identityService = identityService;
        
        if(!partnerSaleRegistrationService){
            throw new TypeError('partnerSaleRegistrationService required');
        }
        this._partnerSaleRegistrationService = partnerSaleRegistrationService;
        
        if(!consumerSaleRegistrationService){
            throw new TypeError('consumerSaleRegistrationService required');
        }
        this._consumerSaleRegistrationService = consumerSaleRegistrationService;
        
        if(!accountService){
            throw new TypeError('accountService required');
        }
        this._accountService = accountService;
        
        if(!partnerRepService){
            throw new TypeError('partnerRepService required');
        }
        this._partnerRepService = partnerRepService;
        
        this.loader = true;
        
        this.draftId = localStorage.getItem('draftId');
        this.installDate = localStorage.getItem('installDate');
        this.productInfo = {};
        
        this._identityService.getAccessToken()
            .then(accessToken => {
                this.accessToken = accessToken;
            
                if( this.installDate !== "null" ){
                    this.flag = "commercial";
                    this._partnerSaleRegistrationService.getPartnerSaleRegistrationInfo( this.draftId , this.accessToken )
                    .then(partnerSaleRegistrationInfo => {
                            this.partnerSaleInfo = partnerSaleRegistrationInfo;
                            this.productInfo.simpleLineItems = this.partnerSaleInfo.simpleLineItems;
                            this.productInfo.compositeLineItems = this.partnerSaleInfo.compositeLineItems;
                            this.getCommercialInfo();
                        }
                    )
                } else {
                    this.flag = "consumer";
                    this._consumerSaleRegistrationService.getconsumerSaleRegistrationInfo( this.draftId , this.accessToken )
                    .then(consumerSaleRegistrationInfo => {
                            this.consumerSaleRegistrationInfo = consumerSaleRegistrationInfo;                            
                            this.productInfo.simpleLineItems = this.consumerSaleRegistrationInfo.simpleLineItems;
                            this.productInfo.compositeLineItems = this.consumerSaleRegistrationInfo.compositeLineItems;
                            this.getConsumerInfo();
                        }
                    )
                }
            });
    }
    /**
     * methods
     */
    getCommercialInfo(){

        var promise1 = this._accountService.getAccountInfo(this.partnerSaleInfo.facilityId, this.accessToken)
            .then(accountInfo => { 
                    this.partnerAccountInfo = accountInfo;                    
                    this.partnerAccountInfo._customerBrand = this.partnerAccountInfo._customerBrand ? this.partnerAccountInfo._customerBrand: "No Match";
                    this.partnerAccountInfo._customerSubBrand = this.partnerAccountInfo._customerSubBrand ? this.partnerAccountInfo._customerSubBrand: "No Match";
                }
            );

        var promise2 =
            this._partnerSaleRegistrationService
                .getPartnerContactInfo(
                    this.partnerSaleInfo.facilityContactId,
                    this.accessToken
                )
                .then(contactInfo => {
                    this.partnercontactInfo = contactInfo;
                }
            );

        var promise3 =
            this._partnerSaleRegistrationService
                .getCustomerSourceInfo( this.accessToken )
                .then(customerSources => {
                    angular.forEach(customerSources , ( val , key ) => {
                        if(val.id == this.partnerSaleInfo.customerSourceId){
                            this.partnerSaleInfo.customerSource = val._name;
                        }
                    });
                }
            );

        var promise4 =
            this._partnerSaleRegistrationService
                .getManagementCompanyInfo( this.accessToken )
                .then(managementCompaniesObj => {
                    var managementCompanies = JSON.parse(managementCompaniesObj.response);
                    angular.forEach(managementCompanies ,( val , key ) => {
                        if(val.id == this.partnerSaleInfo.managementCompanyId){
                            this.partnerSaleInfo.managementCompany = val.name;
                        }
                    });
                }
            );

        var promise5 =
            this._partnerSaleRegistrationService
                .getBuyingGroupInfo( this.accessToken )
                .then(buyingGroupsObj => {
                    var buyingGroupList = JSON.parse(buyingGroupsObj.response);
                    angular.forEach( buyingGroupList , ( val , key ) => {
                        if(val.id == this.partnerSaleInfo.buyingGroupId){
                            this.partnerSaleInfo.buyingGroup = val.name;
                        }
                    });
                }
            );

        var promisesArray = [ promise1, promise2, promise3, promise4, promise5 ];

        if(this.partnerSaleInfo.partnerRepUserId){
            var promise6 = this._partnerRepService.getDealerRep(this.partnerSaleInfo.partnerRepUserId, this.accessToken )
            .then(dealerRep => { 
                    this.partnerSaleInfo.dealerRep = dealerRep.firstName + " " + dealerRep.lastName;
                }
            ).catch( error => {
                    console.log(error.statusText);
                    this.loader = false;
                }
            );
            promisesArray.push( promise6 );
        }

        this._$q
            .all( promisesArray )
            .then((value) =>  {
                    this.loader = false;
                }, (reason) => {
                    console.log(reason);
                    this.loader = false;
                });
    }

    getConsumerInfo(){

        var promise1 = this._consumerSaleRegistrationService.getCountryInfo()

            .then(countries => {
                angular.forEach( countries , ( val , key ) => {
                    if(val.alpha2Code == this.consumerSaleRegistrationInfo.address.countryIso31661Alpha2Code){
                        this.consumerSaleRegistrationInfo.country = val.name;
                    }
                })
            });

        var promise2 = this._consumerSaleRegistrationService
            .getRegionInfo( this.consumerSaleRegistrationInfo.address.countryIso31661Alpha2Code )
            .then(regions => {
                angular.forEach( regions , ( val , key ) => {
                    if(val.code == this.consumerSaleRegistrationInfo.address.regionIso31662Code){
                        this.consumerSaleRegistrationInfo.region = val.name;
                    }
                })
            });

        var promisesArray = [ promise1, promise2 ];

        if(this.consumerSaleRegistrationInfo.partnerRepUserId){            
            var promise3 = this._partnerRepService
                .getDealerRep(this.consumerSaleRegistrationInfo.partnerRepUserId, this.accessToken )
                .then(dealerRep => {
                    this.consumerSaleRegistrationInfo.dealerRep = dealerRep.firstName + " " + dealerRep.lastName;
                }
                ).catch( error => {
                    console.log(error.statusText);
                    this.loader = false;
                    }
                );
            promisesArray.push( promise3 );
        }

        this._$q.all( promisesArray ).then((value) => {
            this.loader = false;
        }, (reason) => {                
            this.loader = true;
        });
    }
}

RegistrationInfoCtrl.$inject = [
    '$scope',
    '$q',
    'identityService',
    'partnerSaleRegistrationService',
    'consumerSaleRegistrationService',
    'accountService',
    'partnerRepService'
];
