export default class SubmittedExtendedWarrantyCtrl {
    
    _$scope;
    
    _$q;
    
    _identityService;
    
    _userInfoService;
    
    _extendedWarrantyService;
    
    _partnerSaleRegistrationService;
    
    _accountService;
    
    _partnerRepService;
    
    _countryAndRegionService;

    constructor(
        $scope,
        $q,
        identityService,
        userInfoService,
        extendedWarrantyService,
        partnerSaleRegistrationService,
        accountService,
        partnerRepService,
        countryAndRegionService
    ){
        if(!$scope){
            throw new TypeError("$scope required");
        }
        this._$scope = $scope;
        
        if(!$q){
            throw new TypeError("$q required");
        }
        this._$q = $q;
        
        if(!identityService){
            throw new TypeError("identityService required");
        }
        this._identityService = identityService;
        
        if(!userInfoService){
            throw new TypeError("userInfoService required");
        }
        this._userInfoService = userInfoService;
        
        if(!extendedWarrantyService){
            throw new TypeError("extendedWarrantyService required");
        }
        this._extendedWarrantyService = extendedWarrantyService;
        
        if(!partnerSaleRegistrationService){
            throw new TypeError("partnerSaleRegistrationService required");
        }
        this._partnerSaleRegistrationService = partnerSaleRegistrationService;
        
        if(!accountService){
            throw new TypeError("accountService required");
        }
        this._accountService = accountService;
        
        if(!partnerRepService){
            throw new TypeError("partnerRepService required");
        }
        this._partnerRepService = partnerRepService;
        
        if(!countryAndRegionService){
            throw new TypeError("countryAndRegionService required");
        }
        this._countryAndRegionService = countryAndRegionService;
        
        this.loader = true;        

        this.registrationId = localStorage.getItem('draftId');
        
        this.promisesArray = [];
        
        identityService.getAccessToken()
            .then(accessToken => {
                    this.accessToken = accessToken;
                    this.getExtendedWarranty( this.registrationId , this.accessToken );
                    this.getPartnerSaleInfo( this.registrationId , this.accessToken );
                }
            )
    }

    /**
     methods
     */
    getExtendedWarranty( registrationId , accessToken ){
        this.EWPromise =
            this._extendedWarrantyService
                .getExtendedWarranty( registrationId , accessToken )
                .then( result => {
                    this._EWInfo = result;
                });
    };
    
    getPartnerSaleInfo( registrationId , accessToken ){
        this._partnerSaleRegistrationService
            .getPartnerSaleRegistrationInfo( registrationId , accessToken )
            .then(partnerSaleRegistrationInfo => {
                this.partnerSaleInfo = partnerSaleRegistrationInfo;
                this.getFacilityInfo(
                    this.partnerSaleInfo.facilityId ,
                    this.partnerSaleInfo.facilityContactId,
                    accessToken
                );
            }
        )
    };
    
    getFacilityInfo( facilityId , facilityContactId, accessToken ){
        var self = this;
        var promise1 =
            this._accountService
                .getAccountInfo( facilityId , accessToken )
                .then(accountInfo => {
                    this.partnerAccountInfo = accountInfo;
                    this._countryAndRegionService
                        .getCountryInfo(this.partnerAccountInfo._address._countryIso31661Alpha2Code)
                        .then( country => {
                            this.partnerAccountInfo._address.country = country;
                        }
                    );
                    this._countryAndRegionService
                        .getRegionInfo(
                            this.partnerAccountInfo._address._regionIso31662Code ,
                            this.partnerAccountInfo._address._countryIso31661Alpha2Code)
                        .then( region => {
                            this.partnerAccountInfo._address.region = region;
                        }
                    )
                });

        var promise2 =
            this._partnerSaleRegistrationService
                .getPartnerContactInfo( facilityContactId , accessToken )
                .then(contactInfo => {
                    this.partnercontactInfo = contactInfo;
                });

        this.promisesArray = [ promise1, promise2 ];

        if(this.partnerSaleInfo.partnerRepUserId){
            var promise3 = this._partnerRepService.getDealerRep( this.partnerSaleInfo.partnerRepUserId, accessToken )
            .then(dealerRep => { 
                    this.partnerSaleInfo.dealerRep = dealerRep.firstName + " " + dealerRep.lastName;
                }
            );

            this.promisesArray.push( promise3 );
        }
        
        this.promisesArray.push( this.EWPromise );

        this._$q
            .all( this.promisesArray )
            .then(function(value) {
                self.loader = false;
                }, function(reason) {
                self.loader = false;
            });
    };
}

SubmittedExtendedWarrantyCtrl.$inject = [
    '$scope',
    '$q',
    'identityService',
    'userInfoService',
    'extendedWarrantyService',
    'partnerSaleRegistrationService',
    'accountService',
    'partnerRepService',
    'countryAndRegionService'
];
