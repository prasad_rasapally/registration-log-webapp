import angular from 'angular';
import 'angular-route';
import 'angular-bootstrap';
import 'angular-messages';
import './bootstrap';
import 'header';
import 'footer';
import RouteConfig from './routeConfig';
import ConfigFactory from './configFactory';
import Config from './config';
import configData from './configData.json!json';
import IdentityServiceSdk from 'identity-service-sdk';
import SessionManager from 'session-manager';
import RegistrationLogServiceSdk from 'registration-log-service-sdk';
import PartnerSaleRegistrationServiceSdk from 'partner-sale-registration-draft-service-sdk';
import ConsumerSaleRegistrationServiceSdk from 'consumer-sale-registration-service-sdk';
import AccountServiceSdk from 'account-service-sdk';
import AccountContactServiceSdk from 'account-contact-service-sdk';
import BuyingGroupServiceSdk from 'buying-group-service-sdk';
import ManagementCompanyServiceSdk from 'management-company-service-sdk';
import CustomerSourceServiceSdk from 'customer-source-service-sdk';
import PartnerRepServiceSdk from 'partner-rep-service-sdk';
import Iso3166Sdk from 'iso-3166-sdk';
import ClaimSpiffServiceSdk from 'claim-spiff-service-sdk';
import ExtendedWarrantyServiceSdk from 'extended-warranty-service-sdk';
import SpiffMasterDataServiceSdk from 'master-data-service-sdk';

import './style.css!css';
import './services/customServices';
import './factories/utilityFactory';
/*
import './filters/customFilters';
*/

angular
    .module(
        "registrationLogWebApp.module",
        [   
            'ngRoute',
            'ui.bootstrap',
            'ngMessages',
            'header.module',
            'footer.module',
            'registrationLogWebApp.services',
            'registrationLogWebApp.factories'/*,
            'registrationLogWebApp.filters'*/
        ]
    )
    .factory(
        'config',
         function(){return ConfigFactory.construct(configData);}
    )
    .factory(
        'identityServiceSdk',
        [
            'config',
             config => new IdentityServiceSdk(config.identityServiceSdkConfig)
        ]
    )
    .factory(
        'sessionManager',
        [
            'config',
             config => new SessionManager(config.sessionManagerConfig)
        ]
    )
    .factory(
        'registrationLogServiceSdk',
        [
            'config',
             config => new RegistrationLogServiceSdk(config.registrationLogServiceSdkConfig)
        ]
    )
    .factory(
        'partnerSaleRegistrationServiceSdk',
        [
            'config',
             config => new PartnerSaleRegistrationServiceSdk(config.partnerSaleRegistrationDraftServiceSdkConfig)
        ]
    )
    .factory(
        'consumerSaleRegistrationServiceSdk',
        [
            'config',
             config => new ConsumerSaleRegistrationServiceSdk(config.consumerSaleRegistrationServiceSdkConfig)
        ]
    )
    .factory(
        'accountServiceSdk',
        [
            'config',
             config => new AccountServiceSdk(config.accountServiceSdkConfig)
        ]
    )
    .factory(
        'accountContactServiceSdk',
        [
            'config',
             config => new AccountContactServiceSdk(config.accountContactServiceSdkConfig)
        ]
    )
    .factory(
        'buyingGroupServiceSdk',
        [
            'config',
             config => new BuyingGroupServiceSdk(config.buyingGroupServiceSdkConfig)
        ]
    )
    .factory(
        'managementCompanyServiceSdk',
        [
            'config',
             config => new ManagementCompanyServiceSdk(config.managementCompanyServiceSdkConfig)
        ]
    )
    .factory(
        'customerSourceServiceSdk',
        [
            'config',
             config => new CustomerSourceServiceSdk(config.customerSourceServiceSdkConfig)
        ]
    )
    .factory(
        'partnerRepServiceSdk',
        [
            'config',
             config => new PartnerRepServiceSdk(config.partnerRepServiceSdkConfig)
        ]
    )
    .factory(
        'claimSpiffServiceSdk',
        [
            'config',
             config => new ClaimSpiffServiceSdk(config.claimSpiffServiceSdkConfig)
        ]
    )
    .factory(
        'spiffMasterDataServiceSdk',
        [
            'config',
             config => new SpiffMasterDataServiceSdk(config.spiffMasterDataServiceSdkConfig)
        ]
    )
    .factory(
        'extendedWarrantyServiceSdk',
        [
            'config',
             config => new ExtendedWarrantyServiceSdk(config.extendedWarrantyServiceSdkConfig)
        ]
    )
    .factory(
        'iso3166Sdk', function() {
            return new Iso3166Sdk();
        }
    )
    .filter('filterByDate', function () {
        return function (records, startDate, endDate) {
            if(records){
                startDate = new Date(startDate).getTime();
                endDate = new Date(endDate).getTime();
                var filteredRecords = [], length = records.length;
                for(var i = 0; i < length; i++){
                    let submittedDate = new Date(records[i]._submittedDate).getTime();
                    if( submittedDate >= startDate && submittedDate <= endDate ){
                        filteredRecords.push(records[i]);
                    }
                }
                return filteredRecords;
            }
        };
    })
    .value(
        'globalValues', {}
    ).run(
        [ 'identityService', 'userInfoService', 'globalValues',
            function ( identityService, userInfoService, globalValues) {
                identityService.getAccessToken()
                    .then(accessToken => {
                            userInfoService.getUserInfo(accessToken)
                                .then(userInfo => {
                                    globalValues.accessToken = accessToken;
                                    globalValues.userInfo = userInfo;
                                });
                        }
                    );
            }
        ]
    ).config(['$routeProvider', $routeProvider => new RouteConfig($routeProvider)]);